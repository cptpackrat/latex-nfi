# latex-nfi
NFI document class for LaTeX.

A slow-growing collection of stuff I always need but never remember;
uses the KOMA-Script `scrartcl` (or `scrreprt` if you need support for
chapters) document class as a base and then adds some customisable
(documented in `nfi.cls`) housekeeping tweaks.

## Usage
Copy `nfi.cls` to your project directory, use as follows:
```latex
\documentclass[redlinks,noside]{nfi}

\nfiTitle{Test Document}
\nfiAuthor{Foo McBarr}
\nfiSubtitle{Optional Elaboration}

\begin{document}

% Your problem.

\end{document}
```
