\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{nfi}

\RequirePackage{xkeyval}

% Option: 'chapters'
% Selects between scrarctl and scrreprt in order to provide chapter support.
\def\baseClass{scrartcl}
\DeclareOptionX{chapters}{
  \gdef\baseClass{scrreprt}
}

%
% Option: 'noside' | 'oneside' | 'twoside'
% Selects between standard one or two-sided layouts with 10mm binding
% offset, or a third option for one-sided with no binding offset.
%
\def\baseSide{oneside}
\def\baseBCOR{0mm}
\DeclareOptionX{oneside}{
  \gdef\baseBCOR{10mm}
}
\DeclareOptionX{twoside}{
  \gdef\baseSide{twoside}
  \gdef\baseBCOR{10mm}
}

%
% Option: 'redlinks'
% Enable colouring of clickable links in output PDFs.
%
\def\redLinks{false}
\DeclareOptionX{redlinks}{
  \def\redLinks{true}
}

%
% Load base KOMA class.
%
\ProcessOptionsX
\LoadClass[
  paper=a4,
  pagesize,
  11pt,
  DIV=10,
  \baseSide,
  BCOR=\baseBCOR,
  cleardoublepage=empty,
  numbers=noenddot,
  titlepage,
  toc=bibliography
]{\baseClass}

%
% Fonts
%
\RequirePackage[charter]{mathdesign}
\RequirePackage[oldstyle,scale=0.8]{sourcecodepro}
\setkomafont{sectioning}{\normalfont\bfseries}
\setkomafont{descriptionlabel}{\normalfont\bfseries}
\setkomafont{caption}{\small}
\setkomafont{captionlabel}{\usekomafont{caption}}

%
% Line Spacing
%
\RequirePackage{setspace}
\onehalfspacing

%
% Penalties
%
\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

%
% Colours
%
\RequirePackage{xcolor}

%
% Graphics and Floats
%
\RequirePackage{float}
\RequirePackage{graphicx}

%
% Title & Headers
%
\RequirePackage{scrlayer-scrpage}
\pagestyle{scrheadings}
\date{}
\cohead{}
\newcommand\nfiAuthor[1]{
  \author{#1}
  \lohead{#1}
}
\newcommand\nfiSubject[1]{
  \subject{#1}
}
\newcommand\nfiTitle[1]{
  \title{#1}
  \rohead{#1}
}
\newcommand\nfiSubtitle[1]{
  \subtitle{#1}
}
\newcommand\nfiFooter[1]{
  \cofoot{}
  \lofoot{#1}
  \rofoot{\thepage}
}

%
% Links & Bookmarks
%
\definecolor{nfi-links}{RGB}{190,10,40}
\RequirePackage[
  colorlinks=\redLinks,
  citecolor=nfi-links,
  linkcolor=nfi-links,
  urlcolor=nfi-links,
]{hyperref}
\RequirePackage[
  sort
]{cleveref}
